// Generated code from Butter Knife. Do not modify!
package com.sun.bingo.ui.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class RichEditorActivity$$ViewInjector {
  public static void inject(Finder finder, final com.sun.bingo.ui.activity.RichEditorActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624058, "field 'toolbar'");
    target.toolbar = (android.support.v7.widget.Toolbar) view;
    view = finder.findRequiredView(source, 2131624086, "field 'actionUndo'");
    target.actionUndo = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624087, "field 'actionRedo'");
    target.actionRedo = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624088, "field 'actionBold'");
    target.actionBold = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624089, "field 'actionItalic'");
    target.actionItalic = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624090, "field 'actionStrikethrough'");
    target.actionStrikethrough = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624091, "field 'actionUnderline'");
    target.actionUnderline = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624092, "field 'actionHeading1'");
    target.actionHeading1 = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624093, "field 'actionHeading2'");
    target.actionHeading2 = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624094, "field 'actionHeading3'");
    target.actionHeading3 = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624095, "field 'actionHeading4'");
    target.actionHeading4 = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624096, "field 'actionTxtColor'");
    target.actionTxtColor = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624097, "field 'actionBgColor'");
    target.actionBgColor = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624098, "field 'actionIndent'");
    target.actionIndent = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624099, "field 'actionOutdent'");
    target.actionOutdent = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624100, "field 'actionAlignLeft'");
    target.actionAlignLeft = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624101, "field 'actionAlignCenter'");
    target.actionAlignCenter = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624102, "field 'actionAlignRight'");
    target.actionAlignRight = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624103, "field 'actionBlockquote'");
    target.actionBlockquote = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624104, "field 'actionInsertImage'");
    target.actionInsertImage = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624105, "field 'actionInsertLink'");
    target.actionInsertLink = (android.widget.ImageButton) view;
    view = finder.findRequiredView(source, 2131624106, "field 'richEditor'");
    target.richEditor = (jp.wasabeef.richeditor.RichEditor) view;
  }

  public static void reset(com.sun.bingo.ui.activity.RichEditorActivity target) {
    target.toolbar = null;
    target.actionUndo = null;
    target.actionRedo = null;
    target.actionBold = null;
    target.actionItalic = null;
    target.actionStrikethrough = null;
    target.actionUnderline = null;
    target.actionHeading1 = null;
    target.actionHeading2 = null;
    target.actionHeading3 = null;
    target.actionHeading4 = null;
    target.actionTxtColor = null;
    target.actionBgColor = null;
    target.actionIndent = null;
    target.actionOutdent = null;
    target.actionAlignLeft = null;
    target.actionAlignCenter = null;
    target.actionAlignRight = null;
    target.actionBlockquote = null;
    target.actionInsertImage = null;
    target.actionInsertLink = null;
    target.richEditor = null;
  }
}
